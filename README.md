# member.data.coop

## Development requirements

- Docker
- Docker compose
- pre-commit (preferred for contributions)

## Start local server

Given that the requirements above are installed, it should be as easy as:

    $ make migrate

This will setup the database. Next run:

    $ make run

This will build the docker image and start the member system on http://localhost:8000.

You can create a superuser by running:

    $ make createsuperuser

Make migrations:

    $ make makemigrations

Make messages:

    $ make makemessages

Running tests:

    $ make test
