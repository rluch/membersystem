DOCKER_COMPOSE = COMPOSE_DOCKER_CLI_BUILD=1 DOCKER_BUILDKIT=1 docker-compose
DOCKER_RUN = ${DOCKER_COMPOSE} run -e UID=`id -u` -e GID=`id -g`
MANAGE = ${DOCKER_RUN} backend python /app/src/manage.py

lint:
	poetry run pre-commit run --all

run:
	${DOCKER_COMPOSE} up --build

build:
	${DOCKER_COMPOSE} build

makemessages:
	${MANAGE} makemessages -a

makemigrations:
	${MANAGE} makemigrations ${EXTRA_ARGS}

migrate:
	${MANAGE} migrate ${EXTRA_ARGS}

createsuperuser:
	${MANAGE} createsuperuser

shell:
	${MANAGE} shell

manage_command:
	${MANAGE} ${COMMAND}

test:
	${DOCKER_RUN} backend pytest src/

add_dependency:
	${DOCKER_RUN} backend poetry add --lock ${DEPENDENCY}

add_dev_dependency:
	${DOCKER_RUN} backend poetry add -D --lock ${DEPENDENCY}
