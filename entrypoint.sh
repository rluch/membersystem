#!/bin/sh

echo "Waiting for postgres..."

POSTGRES_PORT=${POSTGRES_PORT:-5432}
POSTGRES_HOST=${POSTGRES_HOST:-localhost}

while ! nc -z "$POSTGRES_HOST" "$POSTGRES_PORT"; do
  sleep 0.1
done

echo "PostgreSQL started"

# Only migrate, collectstatic and compilemessages if we are NOT in development
if [ -z "$DEBUG" ]; then
  python src/manage.py migrate;
  python src/manage.py collectstatic --no-input;
  python src/manage.py compilemessages;
fi

exec "$@"
