from django import template
from django.urls import reverse

register = template.Library()


@register.simple_tag(takes_context=True)
def active_path(context, path_name, class_name):
    path = reverse(path_name)
    request_path = context.get("request").path

    if path == request_path or ("basepath" in context and context["basepath"] == path):
        return class_name
