from django.contrib.auth.decorators import login_required
from django.shortcuts import render

from .models import Membership


@login_required
def membership_overview(request):
    memberships = Membership.objects.for_user(request.user)
    current_membership = memberships.current()
    previous_memberships = memberships.previous()

    context = dict(
        current_membership=current_membership,
        previous_memberships=previous_memberships,
    )

    return render(request, "membership_overview.html", context)
