from django.shortcuts import render


def index(request):
    return render(request, "index.html")


def services_overview(request):
    return render(request, "services_overview.html")
