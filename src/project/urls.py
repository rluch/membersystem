"""URLs for the membersystem"""
import debug_toolbar
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import include
from django.urls import path
from membership.views import membership_overview

from .views import index
from .views import services_overview

urlpatterns = [
    path("", login_required(index), name="index"),
    path("services/", login_required(services_overview), name="services-overview"),
    path("membership/", membership_overview, name="membership-overview"),
    path("accounts/", include("allauth.urls")),
    path("admin/", admin.site.urls),
    path("__debug__/", include(debug_toolbar.urls)),
]
