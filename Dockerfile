FROM python:3.9-slim-buster

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PYTHONDONTWRITEBYTECODE=1 \
  PYTHONHASHSEED=random \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.1.4

RUN apt-get update \
  && apt-get install -y \
     binutils \
     libpq-dev \
     build-essential \
     netcat-openbsd \
     libcairo2 \
     libpango-1.0-0 \
     libpangocairo-1.0-0 \
     libgdk-pixbuf2.0-0 \
     libffi-dev \
     shared-mime-info \
     gettext \
  && pip install "poetry==$POETRY_VERSION"

WORKDIR /app

COPY poetry.lock pyproject.toml /app/

ARG DJANGO_ENV

RUN poetry export -f requirements.txt $(test "$DJANGO_ENV" != production && echo "--dev") | pip install -r /dev/stdin

RUN groupadd -g 1000 www
RUN useradd -u 1000 -ms /bin/bash -g www www
COPY --chown=www:www ./ /app/
RUN mkdir /app/src/static && chown www:www /app/src/static

ARG BUILD
ENV BUILD ${BUILD}

ENTRYPOINT ["./entrypoint.sh"]

EXPOSE 8000

CMD ["uvicorn", "project.asgi:application", "--host", "0.0.0.0", "--port", "8000", "--workers", "3", "--lifespan", "off", "--app-dir", "/app/src"]
